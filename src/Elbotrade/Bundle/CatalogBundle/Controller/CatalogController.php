<?php

namespace Elbotrade\Bundle\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CatalogController extends Controller
{
    /**
     * @Route("/elbotrade/catalog/index", name="elbotrade-catalog-index")
     * @Template()
     */
    public function indexAction()
    {

        $catalogFinder = $this->get('elbotrade.catalog_bundle.catalog_finder');
        $catalogs = $catalogFinder->findAllCatalog();

        return [
            'catalogs' => $catalogs ? $catalogs : [],
        ];
    }

    /**
     * @Route("/elbotrade/catalog/show/{id}", name="elbotrade-catalog-show")
     * @Template()
     */
    public function showAction($id)
    {

        $catalogFinder = $this->get('elbotrade.catalog_bundle.catalog_finder');
        $catalog = $catalogFinder->findCatalog($id);

        return [
            'catalog' => $catalog,
        ];
    }
}
