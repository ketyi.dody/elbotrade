<?php
namespace Elbotrade\Bundle\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CatalogCategoryController extends Controller
{

    /**
     * @Route("/elbotrade/catalog-category/index", name="elbotrade-catalog_category-index")
     * @Template()
     */
    public function indexAction()
    {

        $catalogCategories = $this->get('elbotrade.catalog.catalog_category_finder')->findActiveCatalogCategories();

        return [
            'catalogCategories' => $catalogCategories ? $catalogCategories : [],
        ];
    }

    /**
     * @Route("/elbotrade/catalog-category/show/{id}", name="elbotrade-catalog_category-show")
     * @Template()
     */
    public function showAction($id)
    {

        $catalogCategory = $this->get('elbotrade.catalog.catalog_category_finder')->findCatalogCategory($id);

        return [
            'catalogCategory' => $catalogCategory,
        ];
    }
}