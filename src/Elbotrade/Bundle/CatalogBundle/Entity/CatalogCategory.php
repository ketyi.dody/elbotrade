<?php
namespace Elbotrade\Bundle\CatalogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * Class CatalogCategory
 * @package Elbotrade\Bundle\CatalogBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="elbo_catalog_category")
 */
class CatalogCategory implements Translatable
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    protected $locale;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @ORM\OneToMany(targetEntity="Elbotrade\Bundle\CatalogBundle\Entity\Catalog", mappedBy="category", cascade={"persist"}, fetch="LAZY")
     */
    protected $catalogs;




    /**
     * CatalogCategory constructor.
     */
    public function __construct()
    {

        $this->catalogs = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function __toString()
    {

        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CatalogCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add catalogs
     *
     * @param \Elbotrade\Bundle\CatalogBundle\Entity\Catalog $catalogs
     * @return CatalogCategory
     */
    public function addCatalog(\Elbotrade\Bundle\CatalogBundle\Entity\Catalog $catalogs)
    {
        $this->catalogs[] = $catalogs;

        return $this;
    }

    /**
     * Remove catalogs
     *
     * @param \Elbotrade\Bundle\CatalogBundle\Entity\Catalog $catalogs
     */
    public function removeCatalog(\Elbotrade\Bundle\CatalogBundle\Entity\Catalog $catalogs)
    {
        $this->catalogs->removeElement($catalogs);
    }

    /**
     * Get catalogs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCatalogs()
    {
        return $this->catalogs;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return CatalogCategory
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return CatalogCategory
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set locale
     *
     * @param $locale
     */
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
}
