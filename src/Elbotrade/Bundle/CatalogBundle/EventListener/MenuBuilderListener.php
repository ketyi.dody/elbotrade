<?php
namespace Elbotrade\Bundle\CatalogBundle\EventListener;

use Sylius\Bundle\WebBundle\Event\MenuBuilderEvent;

class MenuBuilderListener
{

    public function addBackendMenuItems(MenuBuilderEvent $event)
    {

        $menu = $event->getMenu();

        $menu['assortment']->addChild('Catalog Categories', [
            'route' => 'elbotrade_catalog_category_backend_list',
            'labelAttributes' => ['icon' => 'glyphicon glyphicon-folder-close'],
        ])->setLabel('Catalog Categories');
        $menu['assortment']->addChild('Catalogs', [
            'route' => 'elbotrade_catalog_backend_list',
            'labelAttributes' => ['icon' => 'glyphicon glyphicon-list-alt'],
        ])->setLabel('Catalogs');
        $menu['assortment']->addChild('Deals', [
            'route' => 'elbotrade_deal_backend_list',
            'labelAttributes' => ['icon' => 'glyphicon glyphicon-euro'],
        ])->setLabel('Deals');
        $menu['assortment']->addChild('CertificateCategories', [
            'route' => 'elbotrade_certificate_category_backend_list',
            'labelAttributes' => ['icon' => 'glyphicon glyphicon-folder-close'],
        ])->setLabel('Certificate Categories');
        $menu['assortment']->addChild('Certificates', [
            'route' => 'elbotrade_certificate_backend_list',
            'labelAttributes' => ['icon' => 'glyphicon glyphicon-file'],
        ])->setLabel('Certificates');
        $menu['assortment']->addChild('Galleries', [
            'route' => 'elbotrade_gallery_backend_list',
            'labelAttributes' => ['icon' => 'glyphicon glyphicon-picture'],
        ])->setLabel('Galleries');

        $menu->removeChild('sales');
        $menu->removeChild('sales');
//        $menu->removeChild('customer');
        $menu->removeChild('marketing');
        $menu->removeChild('support');
        $menu->removeChild('content');
//        $menu->removeChild('configuration');
    }
}