<?php

namespace Elbotrade\Bundle\CatalogBundle\Service;

use Doctrine\ORM\EntityManager;

class CatalogFinder
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    /**
     * @return array|\Elbotrade\Bundle\CatalogBundle\Entity\Catalog[]
     */
    public function findAllCatalog()
    {

        return $this->entityManager->getRepository('ElbotradeCatalogBundle:Catalog')->findBy(['active' => true]);
    }

    /**
     * @param $id
     * @return \Elbotrade\Bundle\CatalogBundle\Entity\Catalog|null|object
     */
    public function findCatalog($id)
    {
        return $this->entityManager->getRepository('ElbotradeCatalogBundle:Catalog')->find($id);
    }
}