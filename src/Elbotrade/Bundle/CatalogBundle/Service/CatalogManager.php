<?php
namespace Elbotrade\Bundle\CatalogBundle\Service;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\EntityManager;
use Elbotrade\Bundle\CatalogBundle\Entity\Catalog;
use Sonata\MediaBundle\Entity\MediaManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class CatalogManager
{

    /**
     * @var MediaManager
     */
    private $mediaManager;

    /**
     * @var ImageProcessor
     */
    private $imageProcessor;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /** @var string */
    private $fileUploadDir;

    /**
     * @var  Filesystem
     */
    private $filesystem;

    public function __construct(
        MediaManager $mediaManager,
        ImageProcessor $imageProcessor,
        EntityManager $entityManager,
        $fileUploadDir,
        Filesystem $filesystem
    ) {

        $this->mediaManager = $mediaManager;
        $this->imageProcessor = $imageProcessor;
        $this->entityManager = $entityManager;
        $this->fileUploadDir = $fileUploadDir;
        $this->filesystem = $filesystem;

    }

    public function createCatalog()
    {

        return new Catalog();
    }

    public function handleCatalogForm(Form $form, Catalog $catalog, Request $request, $locale)
    {

        if ($catalog !== null) {
            $form->setData($catalog);
        } else {
            $catalog = $this->createCatalog();
        }

        $requestFiles = $request->files->get('elbotrade_bundle_catalogbundle_catalog');

        $form->handleRequest($request);
        $catalog = $form->getData();
        $catalog->setTranslatableLocale($locale);

        $editId = $request->get('editId');
        $catalog = $this->processFiles($requestFiles, $catalog, $request, $editId);

        $this->entityManager->persist($catalog);
        $this->entityManager->flush();

        return $catalog->getId();
    }

    /**
     * @param $requestFiles
     * @param Catalog $catalog
     * @param Request $request
     * @param $editId
     * @return Catalog|void
     */
    private function processFiles($requestFiles, Catalog $catalog, Request $request, $editId)
    {

        if ((\array_key_exists('pdf',
                $requestFiles)) && (!empty($requestFiles['pdf']) || ($requestFiles['pdf'] instanceof \Symfony\Component\HttpFoundation\File\UploadedFile))
        ) {
            $catalog = $this->processPdf($requestFiles, $catalog, $request, $editId);
        }
        if ((\array_key_exists('cover', $requestFiles)) && (\array_key_exists('binaryContent',
                $requestFiles['cover'])) && (!empty($requestFiles['cover']['binaryContent']) || ($requestFiles['cover']['binaryContent'] instanceof \Symfony\Component\HttpFoundation\File\UploadedFile))
        ) {
            $catalog = $this->processCover($requestFiles, $catalog, $request, $editId);
        }

        return $catalog;
    }

    private function processPdf($requestFiles, Catalog $catalog, Request $request, $editId)
    {

        // convert pdf to images
        $folder = $this->fileUploadDir . '/tmp/attachments/' . $editId . '/originals';
        $this->filesystem->mkdir($folder);

        exec('convert -density 400 "' . $requestFiles['pdf'] . '[0]" -resize 1700 ' . $folder . '/pdf_convert.png',
            $output, $return_var);

        if ($return_var != 0) {
            return $catalog;
        }

        // process pdf images
        $catalog = $this->imageProcessor->processImages($editId, 'catalog', $request, $catalog);

//        $media = $catalog->getFirstImage();

        // set cover from first page of pdf
//        $catalog->setCover($media);

        /** @var Media $media */
        $media = $this->mediaManager->create();
        $media->setBinaryContent($requestFiles['pdf']);
        $media->setContext('catalog');
        $media->setProviderName('sonata.media.provider.file');

        // add pdf to catalog
        $catalog->setPdf($media);

        return $catalog;
    }

    private function processCover($requestFiles, Catalog $catalog, Request $request, $editId)
    {

        /** @var Media $media */
        $media = $this->mediaManager->create();
        $media->setBinaryContent($requestFiles['cover']['binaryContent']);
        $media->setContext('catalog');
        $media->setProviderName('sonata.media.provider.image');
        $media->setEnabled(true);

        foreach ($catalog->getImages() as $image) {
            $catalog->removeImage($image);
        }
        $catalog->addImage($media);

        return $catalog;
    }

}