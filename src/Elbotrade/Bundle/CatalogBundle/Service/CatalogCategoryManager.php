<?php
namespace Elbotrade\Bundle\CatalogBundle\Service;

use Doctrine\ORM\EntityManager;
use Elbotrade\Bundle\CatalogBundle\Entity\CatalogCategory;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class CatalogCategoryManager
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * CatalogCategoryManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function handleCatalogCategoryForm(Form $form, CatalogCategory $catalogCategory, Request $request, $language)
    {

        if ($catalogCategory !== null) {
            $form->setData($catalogCategory);
        } else {
            $catalogCategory = $this->createCatalogCategory();
        }

        $form->handleRequest($request);
        $catalogCategory = $form->getData();
        $catalogCategory->setTranslatableLocale($language);


        $this->entityManager->persist($catalogCategory);
        $this->entityManager->flush();

        return $catalogCategory->getId();
    }

    /**
     * @return CatalogCategory
     */
    public function createCatalogCategory()
    {

        return new CatalogCategory();
    }
}