<?php
namespace Elbotrade\Bundle\CatalogBundle\Service;

use Doctrine\ORM\EntityManager;

class CatalogCategoryFinder
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    /**
     * @return array|\Elbotrade\Bundle\CatalogBundle\Entity\CatalogCategory[]
     */
    public function findAllCatalogCategories()
    {

        return $this->entityManager->getRepository('ElbotradeCatalogBundle:CatalogCategory')->findBy(['active' => true]);
    }

    /**
     * @param $id
     * @return \Elbotrade\Bundle\CatalogBundle\Entity\CatalogCategory|null|object
     */
    public function findCatalogCategory($id)
    {

        return $this->entityManager->getRepository('ElbotradeCatalogBundle:CatalogCategory')->find($id);
    }

    /**
     * @return array|\Elbotrade\Bundle\CatalogBundle\Entity\CatalogCategory[]
     */
    public function findActiveCatalogCategories()
    {

        return $this->entityManager->getRepository('ElbotradeCatalogBundle:CatalogCategory')->findBy(['active' => true]);
    }
}