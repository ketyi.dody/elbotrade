<?php
namespace Elbotrade\Bundle\CatalogBundle\Service;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\EntityManager;
use Elastica\Exception\NotFoundException;
use Elbotrade\Bundle\CatalogBundle\Entity\Catalog;
use PunkAve\FileUploaderBundle\Services\FileUploader;
use Sonata\MediaBundle\Entity\MediaManager;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\VarDumper\VarDumper;

class ImageProcessor
{

    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /** @var string */
    private $fileUploadDir;

    /** @var  MediaManager */
    private $mediaManager;

    public function __construct(
        FileUploader $fileUploader,
        EntityManager $entityManager,
        MediaManager $mediaManager,
        $fileUploadDir
    ) {
        $this->fileUploader = $fileUploader;
        $this->entityManager = $entityManager;
        $this->fileUploadDir = $fileUploadDir;
        $this->mediaManager = $mediaManager;
    }

    /**
     * @param $editId
     * @param $context
     * @param Request $request
     * @internal param $galleryName
     * @return Catalog
     */
    public function processImages($editId, $context, Request $request, Catalog $catalog)
    {

        if ($catalog == null) {
            throw new NotFoundException("catalog is empty");
        }

        $files = $this->fileUploader->getFiles(array('folder' => 'tmp/attachments/' . $editId));

        foreach ($files as $file) {
            $info = finfo_open(FILEINFO_MIME_TYPE);
            $filePath = $this->fileUploadDir . '/tmp/attachments/' . $editId . '/originals/' . $file;
            $fileType = explode('/', finfo_file($info, $filePath));
            /** @var Media $media */
            $media = $this->mediaManager->create();
            $media->setBinaryContent($this->fileUploadDir . '/tmp/attachments/' . $editId . '/originals/' . $file);
            $media->setContext($context);
            if ('image' == $fileType[0]) {
                $media->setProviderName('sonata.media.provider.image');
            } else {
                $media->setProviderName('sonata.media.provider.file');
            }
            $media->setEnabled(true);

            $catalog->addImage($media);
        }

        $this->entityManager->persist($catalog);
        $this->entityManager->flush();

        $this->fileUploader->removeFiles(array('folder' => 'tmp/attachments/' . $editId));

        return $catalog;
    }
}