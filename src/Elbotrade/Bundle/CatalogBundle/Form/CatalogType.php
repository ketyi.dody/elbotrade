<?php

namespace Elbotrade\Bundle\CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CatalogType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('active')
            ->add('locale', 'locale')
            ->add('category', 'entity', [
                'class' => 'Elbotrade\Bundle\CatalogBundle\Entity\CatalogCategory',
                'choice_label' => 'name',
                'required' => true,
                'placeholder' => 'Choose an option'
            ])
            ->add('pdf', 'file', ['mapped' => false, 'required' => false])
            ->add('cover', 'sonata_media_type', ['provider' => 'sonata.media.provider.image', 'context' => 'catalog', 'required' => false])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Elbotrade\Bundle\CatalogBundle\Entity\Catalog'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'elbotrade_bundle_catalogbundle_catalog';
    }
}
