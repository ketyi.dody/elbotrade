<?php
namespace Elbotrade\Bundle\CatalogBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CatalogCategoryType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('active')
            ->add('locale', 'locale')
            ->add('catalogs');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Elbotrade\Bundle\CatalogBundle\Entity\CatalogCategory'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'elbotrade_bundle_catalogbundle_catalog_category';
    }

}