<?php
namespace Elbotrade\Bundle\ContentBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CertificateCategoryController extends Controller
{

    /**
     * @Route("/elbotrade/certificate-category/index", name="elbotrade-certificate_category-index")
     * @Template()
     */
    public function indexAction()
    {

        $certificateCategories = $this->get('elbotrade.content_bundle.certificate_category_finder')->findActiveCertificateCategories();

        return [
            'certificateCategories' => $certificateCategories ? $certificateCategories : [],
        ];
    }

    /**
     * @Route("/elbotrade/certificate-category/show/{id}", name="elbotrade-certificate_category-show")
     * @Template()
     */
    public function showAction($id)
    {

        $certificateCategory = $this->get('elbotrade.content_bundle.certificate_category_finder')->findCertificateCategory($id);

        return [
            'certificateCategory' => $certificateCategory,
        ];
    }
}