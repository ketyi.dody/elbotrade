<?php
namespace Elbotrade\Bundle\ContentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class LocaleController extends Controller
{

    /**
     * @return array
     * @Route("/elbotrade/locales", name="elbotrade-locale-get-all")
     * @Template()
     */
    public function getLocalesAction()
    {

        $locales = $this->get('sylius.locale_provider')->getAvailableLocales();
        $currentLocale = $this->get('sylius.context.locale')->getCurrentLocale();

        return [
            'locales' => $locales,
            'currentLocale' => $currentLocale
        ];
    }

    /**
     * @Route("/elbotrade/locale/set/{locale}", name="elbotrade-locale-set")
     * @param Request $request
     * @param $locale
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLocaleAction(Request $request, $locale)
    {

        $this->get('sylius.context.locale')->setCurrentLocale($locale);

        return $this->redirect($request->headers->get('referer'));
    }
}