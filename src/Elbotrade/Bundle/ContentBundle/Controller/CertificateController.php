<?php
namespace Elbotrade\Bundle\ContentBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CertificateController extends Controller
{

    /**
     * @Route("/elbotrade/certificate/index", name="elbotrade-certificate-index")
     * @Template()
     */
    public function indexAction()
    {

        $certificates = $this->get('elbotrade.content_bundle.certificate_finder')->findActiveCertificates();

        return [
            'certificates' => $certificates ? $certificates : [],
        ];
    }

    /**
     * @Route("/elbotrade/certificate/show/{id}", name="elbotrade-certificate-show")
     * @Template()
     */
    public function showAction($id)
    {

        $certificate = $this->get('elbotrade.content_bundle.certificate_finder')->findCertificate($id);

        return [
            'certificate' => $certificate,
        ];
    }
}