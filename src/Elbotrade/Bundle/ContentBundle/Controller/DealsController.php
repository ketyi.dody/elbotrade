<?php
namespace Elbotrade\Bundle\ContentBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DealsController extends Controller
{

    /**
     * @Route("/elbotrade/deal/index", name="elbotrade-deal-index")
     * @Template()
     */
    public function indexAction()
    {

        $deals = $this->get('elbotrade.content_bundle.deal_finder')->findActiveDeals();

        return [
            'deals' => $deals ? $deals : [],
        ];
    }

    /**
     * @Route("/elbotrade/deal/show/{id}", name="elbotrade-deal-show")
     * @Template()
     */
    public function showAction($id)
    {

        $deal = $this->get('elbotrade.content_bundle.deal_finder')->findDeal($id);

        return [
            'deal' => $deal,
        ];
    }
}