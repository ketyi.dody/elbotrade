<?php
namespace Elbotrade\Bundle\ContentBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CertificateCategoryType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('active')
            ->add('locale', 'locale')
            ->add('certificates');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Elbotrade\Bundle\ContentBundle\Entity\CertificateCategory'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'elbotrade_bundle_contentbundle_certificate_category';
    }

}