<?php
namespace Elbotrade\Bundle\ContentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CertificateType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('active')
            ->add('locale', 'locale')
            ->add('pdf', 'sonata_media_type', ['provider' => 'sonata.media.provider.file', 'context' => 'certificate', 'required' => true])
            ->add('category', 'entity',[
                'class' => 'Elbotrade\Bundle\ContentBundle\Entity\CertificateCategory',
                'choice_label' => 'name',
                'required' => true,
                'placeholder' => 'Choose an option'
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Elbotrade\Bundle\ContentBundle\Entity\Certificate'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'elbotrade_bundle_contentbundle_certificate';
    }

}