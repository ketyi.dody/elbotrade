<?php
namespace Elbotrade\Bundle\ContentBundle\Service;

use Doctrine\ORM\EntityManager;
use Elbotrade\Bundle\ContentBundle\Entity\CertificateCategory;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Sonata\MediaBundle\Entity\MediaManager;

class CertificateCategoryManager
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * CertificateCategoryManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function handleCertificateCategoryForm(Form $form, CertificateCategory $certificateCategory, Request $request)
    {

        if ($certificateCategory !== null) {
            $form->setData($certificateCategory);
        } else {
            $certificateCategory = $this->createCertificateCategory();
        }

        $form->handleRequest($request);
        $certificateCategory = $form->getData();


        $this->entityManager->persist($certificateCategory);
        $this->entityManager->flush();

        return $certificateCategory->getId();
    }

    /**
     * @return CertificateCategory
     */
    public function createCertificateCategory()
    {

        return new CertificateCategory();
    }
}