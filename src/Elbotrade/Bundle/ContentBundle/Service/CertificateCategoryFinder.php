<?php
namespace Elbotrade\Bundle\ContentBundle\Service;

use Doctrine\ORM\EntityManager;

class CertificateCategoryFinder
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    /**
     * @return array|\Elbotrade\Bundle\ContentBundle\Entity\CertificateCategory[]
     */
    public function findAllCertificateCategories()
    {

        return $this->entityManager->getRepository('ElbotradeContentBundle:CertificateCategory')->findBy(['active' => true]);
    }

    /**
     * @param $id
     * @return \Elbotrade\Bundle\ContentBundle\Entity\CertificateCategory|null|object
     */
    public function findCertificateCategory($id)
    {

        return $this->entityManager->getRepository('ElbotradeContentBundle:CertificateCategory')->find($id);
    }

    /**
     * @return array|\Elbotrade\Bundle\ContentBundle\Entity\CertificateCategory[]
     */
    public function findActiveCertificateCategories()
    {

        return $this->entityManager->getRepository('ElbotradeContentBundle:CertificateCategory')->findBy(['active' => true]);
    }
}