<?php
namespace Elbotrade\Bundle\ContentBundle\Service;

use Doctrine\ORM\EntityManager;
use Elbotrade\Bundle\ContentBundle\Entity\Deal;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Sonata\MediaBundle\Entity\MediaManager;

class DealManager
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * DealManager constructor.
     * @param EntityManager $entityManager
     * @param MediaManager $mediaManager
     */
    public function __construct(EntityManager $entityManager, MediaManager $mediaManager)
    {

        $this->entityManager = $entityManager;
        $this->mediaManager = $mediaManager;
    }

    public function handleDealForm(Form $form, Deal $deal, Request $request)
    {

        if ($deal !== null) {
            $form->setData($deal);
        } else {
            $deal = $this->createDeal();
        }

        $requestFiles = $request->files->get('elbotrade_bundle_contentbundle_deal');

        $form->handleRequest($request);
        $deal = $form->getData();

        if ((\array_key_exists('image', $requestFiles)) && (\array_key_exists('binaryContent',
                $requestFiles['image'])) && (!empty($requestFiles['image']['binaryContent']) || ($requestFiles['image']['binaryContent'] instanceof \Symfony\Component\HttpFoundation\File\UploadedFile))
        ) {
            $deal = $this->processFiles($requestFiles, $deal);
        }

        $this->entityManager->persist($deal);
        $this->entityManager->flush();

        return $deal->getId();
    }

    /**
     * @return Deal
     */
    public function createDeal()
    {

        return new Deal();
    }

    private function processFiles($requestFiles, Deal $deal)
    {

        $media = $this->mediaManager->create();
        $media->setBinaryContent($requestFiles['image']['binaryContent']);
        $media->setContext('deal');
        $media->setProviderName('sonata.media.provider.image');
        $media->setEnabled(true);

        $deal->setImage($media);

        return $deal;
    }
}