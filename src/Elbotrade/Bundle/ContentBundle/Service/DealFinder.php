<?php
namespace Elbotrade\Bundle\ContentBundle\Service;

use Doctrine\ORM\EntityManager;

class DealFinder
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    /**
     * @return array|\Elbotrade\Bundle\ContentBundle\Entity\Deal[]
     */
    public function findAllDeals()
    {

        return $this->entityManager->getRepository('ElbotradeContentBundle:Deal')->findAll();
    }

    /**
     * @param $id
     * @return \Elbotrade\Bundle\ContentBundle\Entity\Deal|null|object
     */
    public function findDeal($id)
    {

        return $this->entityManager->getRepository('ElbotradeContentBundle:Deal')->find($id);
    }

    /**
     * @return array|\Elbotrade\Bundle\ContentBundle\Entity\Deal[]
     */
    public function findActiveDeals()
    {

        return $this->entityManager->getRepository('ElbotradeContentBundle:Deal')->findBy(['active' => true]);
    }
}