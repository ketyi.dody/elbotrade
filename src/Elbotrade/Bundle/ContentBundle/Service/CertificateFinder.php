<?php
namespace Elbotrade\Bundle\ContentBundle\Service;

use Doctrine\ORM\EntityManager;

class CertificateFinder
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    /**
     * @return array|\Elbotrade\Bundle\ContentBundle\Entity\Certificate[]
     */
    public function findAllCertificates()
    {

        return $this->entityManager->getRepository('ElbotradeContentBundle:Certificate')->findBy(['active' => true]);
    }

    /**
     * @param $id
     * @return \Elbotrade\Bundle\ContentBundle\Entity\Certificate|null|object
     */
    public function findCertificate($id)
    {

        return $this->entityManager->getRepository('ElbotradeContentBundle:Certificate')->find($id);
    }

    /**
     * @return array|\Elbotrade\Bundle\ContentBundle\Entity\Certificate[]
     */
    public function findActiveCertificates()
    {

        return $this->entityManager->getRepository('ElbotradeContentBundle:Certificate')->findBy(['active' => true]);
    }
}