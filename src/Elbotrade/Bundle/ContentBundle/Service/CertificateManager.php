<?php
namespace Elbotrade\Bundle\ContentBundle\Service;

use Doctrine\ORM\EntityManager;
use Elbotrade\Bundle\ContentBundle\Entity\Certificate;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Sonata\MediaBundle\Entity\MediaManager;

class CertificateManager
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * CertificateManager constructor.
     * @param EntityManager $entityManager
     * @param MediaManager $mediaManager
     */
    public function __construct(EntityManager $entityManager, MediaManager $mediaManager)
    {

        $this->entityManager = $entityManager;
        $this->mediaManager = $mediaManager;
    }

    public function handleCertificateForm(Form $form, Certificate $certificate, Request $request)
    {

        if ($certificate !== null) {
            $form->setData($certificate);
        } else {
            $certificate = $this->createCertificate();
        }

        $requestFiles = $request->files->get('elbotrade_bundle_contentbundle_certificate');

        $form->handleRequest($request);
        $certificate = $form->getData();

        if ((\array_key_exists('pdf', $requestFiles)) && (\array_key_exists('binaryContent',
                $requestFiles['pdf'])) && (!empty($requestFiles['pdf']['binaryContent']) || ($requestFiles['pdf']['binaryContent'] instanceof \Symfony\Component\HttpFoundation\File\UploadedFile))
        ) {
            $certificate = $this->processFiles($requestFiles, $certificate);
        }

        $this->entityManager->persist($certificate);
        $this->entityManager->flush();

        return $certificate->getId();
    }

    /**
     * @return Certificate
     */
    public function createCertificate()
    {

        return new Certificate();
    }

    private function processFiles($requestFiles, Certificate $certificate)
    {

        $media = $this->mediaManager->create();
        $media->setBinaryContent($requestFiles['pdf']['binaryContent']);
        $media->setContext('default');
        $media->setProviderName('sonata.media.provider.file');
        $media->setEnabled(true);

        $certificate->setPdf($media);

        return $certificate;
    }
}