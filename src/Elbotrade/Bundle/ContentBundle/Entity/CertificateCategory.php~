<?php
namespace Elbotrade\Bundle\ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CertificateCategory
 * @package Elbotrade\Bundle\ContentBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="elbo_certificate_category")
 */
class CertificateCategory
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @ORM\Column(type="text")
     */
    protected $locale;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @ORM\OneToMany(targetEntity="Elbotrade\Bundle\ContentBundle\Entity\Certificate", mappedBy="category", cascade={"persist"}, fetch="LAZY")
     */
    protected $certificates;

    public function __construct()
    {

        $this->certificates = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CertificateCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add certificates
     *
     * @param \Elbotrade\Bundle\ContentBundle\Entity\Certificate $certificates
     * @return CertificateCategory
     */
    public function addCertificate(\Elbotrade\Bundle\ContentBundle\Entity\Certificate $certificates)
    {
        $this->certificates[] = $certificates;

        return $this;
    }

    /**
     * Remove certificates
     *
     * @param \Elbotrade\Bundle\ContentBundle\Entity\Certificate $certificates
     */
    public function removeCertificate(\Elbotrade\Bundle\ContentBundle\Entity\Certificate $certificates)
    {
        $this->certificates->removeElement($certificates);
    }

    /**
     * Get certificates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCertificates()
    {
        return $this->certificates;
    }
}
