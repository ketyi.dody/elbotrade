<?php
namespace Elbotrade\Bundle\ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Certificate
 * @package Elbotrade\Bundle\ContentBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="elbo_certificate")
 */
class Certificate
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="pdf_id", referencedColumnName="id")
     */
    protected $pdf;

    /**
     * @ORM\Column(type="text")
     */
    protected $locale;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateAdded;

    /**
     * @ORM\ManyToOne(targetEntity="Elbotrade\Bundle\ContentBundle\Entity\CertificateCategory", inversedBy="certificates", cascade={"persist"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    public function __construct()
    {

        $this->dateAdded = new \DateTime('now');
    }

    public function __toString()
    {

        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Certificate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return Certificate
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Certificate
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     * @return Certificate
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime 
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set pdf
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $pdf
     * @return Certificate
     */
    public function setPdf(\Application\Sonata\MediaBundle\Entity\Media $pdf = null)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media 
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set category
     *
     * @param \Elbotrade\Bundle\ContentBundle\Entity\CertificateCategory $category
     * @return Certificate
     */
    public function setCategory(\Elbotrade\Bundle\ContentBundle\Entity\CertificateCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Elbotrade\Bundle\ContentBundle\Entity\CertificateCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
