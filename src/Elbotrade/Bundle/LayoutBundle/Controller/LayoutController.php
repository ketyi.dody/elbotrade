<?php

namespace Elbotrade\Bundle\LayoutBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class LayoutController extends Controller
{
    /**
     * @Route("/layout")
     * @Template()
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route("/contact", name="elbotrade-contact")
     * @Template()
     */
    public function contactAction()
    {
        return [];
    }

    /**
     * @param string $type
     * @Route("/services/{type}", name="elbotrade-services")
     * @return string
     */
    public function servicesAction($type)
    {

        if ($type == 'small') {
            return $this->render(
                'ElbotradeLayoutBundle:Layout/services:small.html.twig'
            );
        }

        if ($type == 'big') {
            return $this->render(
                'ElbotradeLayoutBundle:Layout/services:big.html.twig'
            );
        }

        return $this->render(
            '@ElbotradeLayout/Partial/404.html.twig'
        );
    }
}
