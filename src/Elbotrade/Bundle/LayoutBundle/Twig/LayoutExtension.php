<?php
/**
 * Created by PhpStorm.
 * User: dido
 * Date: 5/1/16
 * Time: 9:41 PM
 */

namespace Elbotrade\Bundle\LayoutBundle\Twig;


use Symfony\Component\Translation\Translator;

class LayoutExtension extends \Twig_Extension
{

    /**
     * @var Translator
     */
    protected $translator;

    public function __construct(Translator $translator)
    {

        $this->translator = $translator;
    }

    public function getFilters()
    {

        return [
            new \Twig_SimpleFilter('language_flags', [$this, 'getLanguageFlags']),
        ];
    }

    public function getLanguageFlags($language)
    {

        $languageCode = substr($language, 0, 2);
        return $this->translator->trans($languageCode, [], 'menu');
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'layout_extension';
    }
}