<?php
namespace Elbotrade\Bundle\GalleryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class GalleryController extends Controller
{

    /**
     * @Route("/elbotrade/gallery/list", name="elbotrade_gallery_list")
     * @Template()
     */
    public function listAction()
    {

        $galleries = $this->get('elbotrade.gallery_bundle.media_finder')->findAllGallery();

        return [
            'galleries' => $galleries
        ];
    }

    /**
     * @Route("/elbotrade/gallery/show/{galleryId}", name="elbotrade_gallery_show")
     * @Template()
     */
    public function showAction($galleryId)
    {

        $gallery = $this->get('elbotrade.gallery_bundle.media_finder')->findGallery($galleryId);

        return [
            'gallery' => $gallery
        ];
    }
}