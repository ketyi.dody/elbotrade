<?php
namespace Elbotrade\Bundle\GalleryBundle\Service;

use Doctrine\ORM\EntityManager;

class GalleryFinder
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * GalleryFinder constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    /**
     * @return \Application\Sonata\MediaBundle\Entity\Media[]|array
     */
    public function findAllMedia()
    {

        return $this->entityManager->getRepository('ApplicationSonataMediaBundle:Media')->findAll();
    }

    /**
     * @param $mediaId
     * @return \Application\Sonata\MediaBundle\Entity\Media|null|object
     */
    public function findMedia($mediaId)
    {

        return $this->entityManager->getRepository('ApplicationSonataMediaBundle:Media')->find($mediaId);
    }

    /**
     * @return \Application\Sonata\MediaBundle\Entity\Gallery[]|array
     */
    public function findAllGallery()
    {

        return $this->entityManager->getRepository('ApplicationSonataMediaBundle:Gallery')->findAll();
    }

    public function findGallery($galleryId)
    {

        return $this->entityManager->getRepository('ApplicationSonataMediaBundle:Gallery')->find($galleryId);
    }

    public function findGalleryHasMedia($galleryHasMediaId)
    {

        return $this->entityManager->getRepository('ApplicationSonataMediaBundle:GalleryHasMedia')->find($galleryHasMediaId);
    }
}