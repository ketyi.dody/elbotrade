<?php
namespace Elbotrade\Bundle\GalleryBundle\Service;

use Application\Sonata\MediaBundle\Entity\Gallery;
use Application\Sonata\MediaBundle\Entity\GalleryHasMedia;
use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\EntityManager;
use Imagick;
use Sonata\MediaBundle\Provider\ImageProvider;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Sonata\MediaBundle\Entity\MediaManager;

class GalleryManager
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $kernelRootDir;

    /**
     * @var MediaManager
     */
    protected $mediaManager;

    /**
     * @var ImageProvider
     */
    protected $imageProvider;

    public function __construct(EntityManager $entityManager, MediaManager $mediaManager, $kernelRootDir, ImageProvider $imageProvider)
    {

        $this->entityManager = $entityManager;
        $this->kernelRootDir = $kernelRootDir;
        $this->mediaManager = $mediaManager;
        $this->imageProvider = $imageProvider;
    }

    public function createGallery()
    {

        return new Gallery();
    }

    public function removeGallery(Gallery $gallery)
    {

        /** @var GalleryHasMedia $galleryHasMedia */
        foreach ($gallery->getGalleryHasMedias() as $galleryHasMedia) {
            $this->removeGalleryHasMedia($galleryHasMedia);
            $this->removeMedia($galleryHasMedia->getMedia());
        }

        $this->entityManager->remove($gallery);

        $this->entityManager->flush();
    }

    public function saveGallery(Gallery $gallery)
    {

        $this->entityManager->persist($gallery);
        $this->entityManager->flush();
    }

    public function createMedia()
    {

        return new Media();
    }

    public function removeMedia($media)
    {

        $this->imageProvider->removeThumbnails($media);
        $this->mediaManager->delete($media);
    }

    public function createGalleryHasMedia()
    {

        return new GalleryHasMedia();
    }

    public function removeGalleryHasMedia($galleryHasMedia)
    {

        $this->entityManager->remove($galleryHasMedia);
    }

    public function handleGalleryForm(Form $form, Gallery $gallery, Request $request)
    {

        if ($gallery !== null) {
            $form->setData($gallery);
        } else {
            $gallery = $this->createGallery();
        }

        $requestFiles = $request->files->get('application_bundle_media_gallery');
        $data = $request->request->get('application_bundle_media_gallery');

        $gallery->setName($data['name']);
        $gallery->setDefaultFormat('big');

        if (!(null == $requestFiles['media'][0])) {
            $gallery = $this->handleGalleryFiles($requestFiles, $gallery);
        }

        return $gallery;
    }

    private function handleGalleryFiles($requestFiles, Gallery $gallery)
    {

        /** @var UploadedFile $updloadedFile */
        foreach ($requestFiles['media'] as $uploadedFile) {
            $uploadDir = $this->kernelRootDir."/../web/uploads/tmp/attachments";
            $fileName = $uploadedFile->getClientOriginalName();
            $file = $uploadedFile->move($uploadDir, $fileName);
            $this->checkOrientation($file);
            $media = $this->createMedia();
            $media->setContext('default');
            $media->setProviderName('sonata.media.provider.image');
            $media->setEnabled(true);
            $media->setBinaryContent($file);

            $this->mediaManager->save($media);

            unlink($file);

            $galleryHasMedia = $this->createGalleryHasMedia();
            $galleryHasMedia->setMedia($media);

            $gallery->addGalleryHasMedias($galleryHasMedia);
        }

        return $gallery;
    }

    private function checkOrientation($file)
    {

        $exif = exif_read_data($file);
        $orientation = $exif['Orientation'];

        if (in_array($orientation, [3, 6, 8])) {
            $this->resample($file, $orientation);
        }
    }

    private function resample($file, $orientation)
    {

        $newFile = imagecreatefromjpeg($file);

        switch($orientation) {
            case 3:
                $newFile = imagerotate($newFile, 180, 0);
                break;
            case 6:
                $newFile = imagerotate($newFile, -90, 0);
                break;
            case 8:
                $newFile = imagerotate($newFile, 90, 0);
                break;
        }

        imagejpeg($newFile, $file, 90);
    }

}