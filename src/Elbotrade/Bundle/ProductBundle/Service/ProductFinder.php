<?php
namespace Elbotrade\Bundle\ProductBundle\Service;

use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductRepository;

class ProductFinder
{

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function findByTaxon($taxon)
    {

        $qb = $this->productRepository->createQueryBuilder('p');
        $qb->select('p')
            ->where(':taxon MEMBER OF p.taxons')
            ->setParameter('taxon', $taxon)
        ;

        return $qb->getQuery()->getResult();
    }
}