<?php
namespace Elbotrade\Bundle\ProductBundle\Service;


use Sylius\Bundle\TaxonomyBundle\Doctrine\ORM\TaxonomyRepository;
use Sylius\Bundle\TaxonomyBundle\Doctrine\ORM\TaxonRepository;

class ProductCategoryFinder
{

    /**
     * @var TaxonomyRepository
     */
    protected $taxonomyRepository;

    /**
     * @var TaxonRepository
     */
    protected $taxonRepository;

    /**
     * ProductCategoryFinder constructor.
     * @param TaxonomyRepository $taxonomyRepository
     * @param TaxonRepository $taxonRepository
     */
    public function __construct(TaxonomyRepository $taxonomyRepository, TaxonRepository $taxonRepository)
    {

        $this->taxonomyRepository = $taxonomyRepository;
        $this->taxonRepository = $taxonRepository;
    }

    /**
     * Full category list
     * @return array
     */
    public function getFullCategoryList()
    {

        $taxonomies = $this->taxonomyRepository->findAll();

        if (empty($taxonomies)) {
            return;
        }

        return $taxonomies;
    }

    public function getTaxonsByTaxonomy($taxonomyId)
    {

        $taxonomy = $this->taxonomyRepository->find($taxonomyId);
        $taxons = $taxonomy->getTaxons();

        return $taxons;
    }

    public function getTaxonsByParent($taxonId)
    {

        $taxons = $this->taxonRepository->findBy(['parent' => $taxonId]);

        return $taxons;
    }
}