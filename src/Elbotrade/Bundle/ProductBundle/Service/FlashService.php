<?php
namespace Elbotrade\Bundle\ProductBundle\Service;


use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class FlashService
{

    /**
     * @var FlashBag
     */
    private $flashBag;

    public function __construct(FlashBag $flashBag)
    {

        $this->flashBag = $flashBag;
    }

    public function setFlash($type, $message)
    {

        $this->flashBag->add($type, $message);
    }
}