<?php
namespace Elbotrade\Bundle\ProductBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sylius\Component\Core\Model\Taxon;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\ProductVariant;
use Sylius\Component\Core\Model\ProductVariantImage;
use Sylius\Component\Product\Model\Variant;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class ProductImportController extends Controller
{

    /**
     * Import products from <project dir>/web/media/image/products
     * Folder with no images -> taxon (category)
     * Folder with images included -> product
     *
     * @Route("/product/import/{taxonId}", name="elbotrade-products-import")
     * @param $taxonId
     * @return Response
     */
    public function importProductAction($taxonId)
    {

        $taxonRepository = $this->get('sylius.repository.taxon');
        /** @var Taxon $taxonomy */
        $parentTaxon = $taxonRepository->find($taxonId);
        $taxonomy = $parentTaxon->getTaxonomy();

        $productsDir = $this->get('kernel')->getRootDir() . '/..' . $this->getParameter('media_path') . 'products/';
        $dirs = glob($productsDir . '*', GLOB_BRACE);
        $entityManager = $this->get('doctrine.orm.entity_manager');

        $this->findDirs($dirs, $taxonomy, $parentTaxon, $productsDir, $entityManager);

        $entityManager->flush();

        return $this->render(
            '@ElbotradeLayout/Partial/success.html.twig',
            [
                'operation' => 'product import'
            ]
        );
    }

    /**
     * Deletes products with ID higher than parameter
     *
     * @param $productId
     *
     * @Route("/product/delete/{productId}", name="elbotrade-products-delete")
     * @return Response
     */
    public function deleteProductHigherThanAction($productId)
    {

        $em = $this->getDoctrine()->getManager();
        $productRepository = $this->get('sylius.repository.product');
        $imageUploader = $this->get('sylius.image_uploader');
        $filesystem = $this->get('filesystem');

        $qb = $productRepository->createQueryBuilder('p');
        $qb->select('p')
            ->where('p.id > :productId')
            ->setParameter('productId', $productId);

        $products = $qb->getQuery()->getResult();

        /** @var Product $product */
        foreach ($products as $product) {
            /** @var Variant $variant */
            foreach ($product->getVariants() as $variant) {
                $em->remove($variant);
            }
            /** @var ProductVariantImage $image */
            foreach ($product->getImages() as $image) {
                if ($filesystem->exists($image->getPath())) {
                    $imageUploader->remove($image->getPath());
                }
                $em->remove($image);
            }
            $em->remove($product);
        }

        $em->flush();

        return $this->render(
            'ElbotradeLayoutBundle:Partial:success.html.twig',
            [
                'operation' => 'delete'
            ]
        );
    }

    /**
     * Recursive function for finding directories, decide if it is a taxon or a product -> create it
     *
     * @param array $dirs
     * @param $taxonomy
     * @param $taxonParent
     * @param $productsDir
     * @param EntityManager $entityManager
     */
    private function findDirs(array $dirs, $taxonomy, $taxonParent, $productsDir, EntityManager $entityManager)
    {
        foreach ($dirs as $dir) {
            if (empty($images = glob($dir . '/*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE))) {
                $array = explode('/', $dir);
                $taxonName = is_array($array) ? end($array) : 'asdf';
                $taxon = $this->createTaxon($taxonName, $taxonomy, $taxonParent, $entityManager);
                $this->findDirs(glob($dir . '/*', GLOB_BRACE), $taxonomy, $taxon, $productsDir, $entityManager);
            }

            $array = explode('/', $dir);
            $productName = end($array);

            $taxons = new ArrayCollection();
            if ($taxonParent != null) $taxons->add($taxonParent);

            $productRepository = $this->get('sylius.repository.product');
            /** @var Product $product */
            $product = $productRepository->createNew();
            $product->setName($productName);
            $product->setDescription($productName);
            $product->setTaxons($taxons);
            $product->setPrice(1);

            $imagesDir = $productsDir . $productName . '/';

            /** @var ProductVariant $masterVariant */
            $masterVariant = $product->getMasterVariant();

            $masterVariant->setPrice(1);
            foreach ($images as $image) {
                $file = new \Symfony\Component\HttpFoundation\File\File($image);

                $productVariantImage = new ProductVariantImage();
                $productVariantImage->setFile($file);
                $masterVariant->addImage($productVariantImage);

            }

            $uploader = $this->get('sylius.image_uploader');
            /** @var ProductVariantImage $image */
            foreach ($masterVariant->getImages() as $image) {
                $uploader->upload($image);
            }

            $entityManager->persist($product);
        }
    }

    /**
     * Taxon (product category) create function
     *
     * @param $taxonName
     * @param $taxonomy
     * @param $taxonParent
     * @param EntityManager $entityManager
     * @return null|object|Taxon
     */
    private function createTaxon($taxonName, $taxonomy, $taxonParent, EntityManager $entityManager)
    {
        $taxonsRepository = $this->get('sylius.repository.taxon');
        if (!$taxon = $taxonsRepository->findOneBy(['name' => $taxonName])) {
            /** @var Taxon $taxon */
            $taxon = $taxonsRepository->createNew();
            $taxon->setTaxonomy($taxonomy);
            $taxon->setParent($taxonParent);
            $taxon->setName($taxonName);
            $entityManager->persist($taxon);
            $entityManager->flush();
        }

        return $taxon;
    }
}