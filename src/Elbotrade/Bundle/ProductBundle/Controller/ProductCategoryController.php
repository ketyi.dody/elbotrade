<?php
namespace Elbotrade\Bundle\ProductBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ProductCategoryController extends Controller
{

    /**
     * @Template()
     * @return array
     * @Route("/product/categories", name="category-list")
     */
    public function getCategoryListAction()
    {

        $taxonomies = $this->get('elbotrade_product.services.product_category_finder')->getFullCategoryList();

        return $this->render(
            'ElbotradeProductBundle:Product:getCategoryList.html.twig',
            [
                'categories' => $taxonomies
            ]
        );
    }

    /**
     * @param int $taxonomyId
     * @param int $taxonId
     * @return Response
     * @internal param $categoryId
     * @Route("/product/sub-categories/{taxonomyId}/{taxonId}", name="sub-category-list")
     */
    public function getCategoryTreeForCategoryAction($taxonomyId, $taxonId = 0)
    {

        $productCategoryFinder = $this->get('elbotrade_product.services.product_category_finder');

        if (0 == $taxonId) {
            $taxons = $productCategoryFinder->getTaxonsByTaxonomy($taxonomyId);
        } else {
            $taxons = $productCategoryFinder->getTaxonsByParent($taxonId);
        }

        if (empty($taxons)) {
            return $this->render('@ElbotradeLayout/Partial/404.html.twig');
        }

        return $this->render(
            'ElbotradeProductBundle:Product:getCategoryTreeForCategory.html.twig',
            [
                'subCategories' => $taxons
            ]
        );
    }

    /**
     * @Template()
     * @Route("/products/category-menu", name="products-category-menu-list")
     */
    public function getCategoryMenuListAction()
    {

        $taxonomies = $this->get('elbotrade_product.services.product_category_finder')->getFullCategoryList();

        return $this->render(
            'ElbotradeProductBundle:Product:getCategoryMenuList.html.twig',
            [
                'categories' => $taxonomies
            ]
        );
    }
}