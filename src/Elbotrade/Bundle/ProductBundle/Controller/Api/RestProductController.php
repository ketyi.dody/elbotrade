<?php
namespace Elbotrade\Bundle\ProductBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\Taxon;
use Symfony\Component\HttpFoundation\JsonResponse;

class RestProductController extends FOSRestController
{

    /**
     * @param $taxonId
     * @return null|JsonResponse
     *
     * @Route("/list/{taxonId}", name="elbotrade-api-product-list")
     * @Method("GET")
     */
    public function getProductList($taxonId)
    {

        $taxonRepository = $this->get('sylius.repository.taxon');
        $productFinder = $this->get('elbotrade_product.services.product_finder');
        /** @var Taxon $taxon */
        $taxon = $taxonRepository->find($taxonId);

        if ($taxon->getPermalink() == 'kaindl/online-konfigurator') {
            return $this->render('@ElbotradeLayout/Partial/configurator.html.twig');
        }

        $products = $productFinder->findByTaxon($taxon);

        if (empty($products)) return $this->render('@ElbotradeLayout/Partial/404_content.html.twig');

        return $this->render(
            'ElbotradeProductBundle:Product:getProductListByCategory.html.twig',
            [
                'products' => $products,
                'categoryId' => $taxonId
            ]
        );
    }
}