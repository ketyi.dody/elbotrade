<?php
namespace Elbotrade\Bundle\ProductBundle\Controller;

use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\Taxon;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ProductController extends Controller
{

    /**
     * @Template()
     * @param $category
     * @return Response
     * @Route("/products/{category}", name="products-by-category")
     */
    public function getProductListByCategoryAction($category)
    {

        $productFinder = $this->get('elbotrade_product.services.product_finder');
        /** @var Taxon $taxon */
        $taxon = $this->get('sylius.repository.taxon')->find($category);

        if (is_null($taxon)) {
            return $this->render('@ElbotradeLayout/Partial/404.html.twig');
        }

        $products = $productFinder->findByTaxon($taxon);

        return $this->render(
            'ElbotradeProductBundle:Product:getProductListByCategory.html.twig',
            [
                'categoryId' => $category,
                'products' => $products
            ]
        );
    }

    /**
     * @param $productId
     * @Route("/images/list/{productId}", name="elbotrade-product-images-list")
     * @return Response
     */
    public function getProductImagesAction($productId)
    {

        $productRepository = $this->get('sylius.repository.product');
        /** @var Product $product */
        $product = $productRepository->find($productId);

        return $this->render(
            'ElbotradeProductBundle:Product:getProductImages.html.twig',
            [
                'product' => $product
            ]
        );
    }
}