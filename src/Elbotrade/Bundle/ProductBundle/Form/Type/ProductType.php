<?php
namespace Elbotrade\Bundle\ProductBundle\Form\Type;

use Sylius\Bundle\CoreBundle\Form\Type\ProductType as BaseProductType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductType extends BaseProductType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('media', 'sonata_media_type', array(
                'required'    => false,
                'label'       => 'PDF',
                'provider' => 'sonata.media.provider.file',
                'context'  => 'default'
            ));

    }
}