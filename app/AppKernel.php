<?php

use Sylius\Bundle\CoreBundle\Kernel\Kernel;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new \Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle(),
            new Elbotrade\Bundle\CatalogBundle\ElbotradeCatalogBundle(),

            // Sonata media
            new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\MediaBundle\SonataMediaBundle(),
            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
            new Application\Sonata\MediaBundle\ApplicationSonataMediaBundle(),
            new PunkAve\FileUploaderBundle\PunkAveFileUploaderBundle(),
            new Elbotrade\Bundle\LayoutBundle\ElbotradeLayoutBundle(),
            new Elbotrade\Bundle\ContentBundle\ElbotradeContentBundle(),
            new Elbotrade\Bundle\GalleryBundle\ElbotradeGalleryBundle(),
            new Elbotrade\Bundle\ProductBundle\ElbotradeProductBundle(),
        );

        if (in_array($this->environment, array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
        }

        return array_merge(parent::registerBundles(), $bundles);
    }
}
